#pragma once

#include <new>
#include <cstdlib>
#include <cstddef>

// FIXME: don't define this to the global namespace
#ifdef _WIN32
#define posix_memalign(p, a, s) (((*(p)) = _aligned_malloc((s), (a))), *(p) ?0 :errno)
#endif

namespace sci {
static constexpr std::size_t BYTES_ALIGNED = 64;

template <typename T>
class AlignedAllocator {
  public:
  typedef T value_type;
   AlignedAllocator() = default;

   [[nodiscard]] T* allocate(const size_t elem) {
       void* ptr(NULL);
       int error = posix_memalign(&ptr, BYTES_ALIGNED, elem * sizeof(T));
       if (error != 0) { throw std::bad_alloc(); }
       return reinterpret_cast<T*>(ptr);
   }

   // TODO: what is n for??
   void deallocate(T* ptr, std::size_t n) noexcept {
     std::free(ptr);
   }
};

} //end namespace sci

