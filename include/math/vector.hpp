#pragma once

#include <alloc/buffer.hpp>

#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstring>
#include <iostream>
#include <cassert>

namespace sci {
template <typename type>
class Vector {
  public:
   Vector() = default;

   // size-based constructor
   explicit Vector(const size_t size) :
     _buf(size), _size(size)
   {}

   // initializer list constructors
   // TODO: use something like std::optional and handle bad sizes passed in
   // ie, sizes that don't vecch the initializer list given
   explicit Vector(const std::initializer_list<type>&& l) :
     _buf(std::move(l)), _size(l.size())
   {}

   // copy constructor
   Vector(const Vector& vec) noexcept :
     _buf(vec._buf), _size(vec._size)
   {}

   // move constructor
   Vector(const Vector&& vec) noexcept :
     _buf(std::move(vec._buf)), _size(vec._size)
   {}

   size_t size() const { return _size; }

   void set(const char val) {
     memset(_buf.data(), val, _size * sizeof(type));
   }
   void fill(const type val) {
     std::fill(_buf.begin(), _buf.end(), val);
   }

   // destructor
   ~Vector() = default;

   Vector& operator+=(const Vector& vec2) {
     assert(_size == vec2._size);
     type* res(_buf.data());
     const type* inp(vec2._buf.data());
     #pragma clang loop vectorize(enable) interleave(enable)
     //#pragma clang loop vectorize_width(4) interleave_count(4)
     for (size_t idx = 0; idx < _size; ++idx) {
       *res++ += *inp++;
       //_buf[idx] += vec2._buf[idx];
     }
     return *this;
   }
   Vector<type> operator+(const Vector& vec2) const {
     Vector<type> result(*this);
     result += vec2;
     return result;
   }

   Vector<type> operator*(const Vector& vec2) const {
     assert(_size == vec2._size);
     Vector result(_size);
     std::transform(_buf.begin(), _buf.end(), vec2._buf.begin(), result._buf.begin(),
                    [](const type& x1, const type& y1) -> type {return x1*y1; });
     return result;
   }
   Vector<type> operator*=(const Vector& vec2) const {
     std::transform(_buf.begin(), _buf.end(), vec2._buf.begin(), _buf.begin(),
                    [](const type& x1, const type& y1) -> type {return x1*y1; });
   }

   operator std::string() const {
     std::ostringstream ss;
     for (const auto& val : _buf) {
       ss << val << ", ";
     }
     return ss.str();
   }

   bool operator==(const Vector& vec2) {
     return (_size == vec2._size) && (_buf == vec2._buf);
   }
   type& operator[](const size_t idx) { return _buf[idx]; }
   const type& operator[](const size_t idx) const {
     return _buf[idx];
   }

   // TODO: write swap function
   Vector& operator=(Vector rhs) {
     swap(rhs);
     return *this;
   }

  private:
   Buffer<type> _buf;
   size_t _size;
};

template <typename T>
inline bool operator!=(const Vector<T>& lhs, const Vector<T>& rhs) {
  return !operator==(lhs,rhs);
}
template <typename T>
std::ostream& operator<<(std::ostream& os, const T& obj) {
  // write obj to stream
  os << operator std::string(obj);

  return os;
}

} //end namespace sci

