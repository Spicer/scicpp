#pragma once

#include <algorithm>
#include <vector>
#include <utility>
#include <numeric>
#include <chrono>
#include <cmath>

template<class T>
double find_mean(const std::vector<T>& vec) {
  return std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
}

template<class T>
std::pair<double, double> find_mean_stdev(const std::vector<T>& vec) {
  double mean(find_mean(vec));
  double accum(0.0);
  std::for_each(vec.begin(), vec.end(), [&](const T& val) {
    accum += (val - mean) * (val - mean);
  });
  return std::make_pair(mean, sqrt(accum / vec.size()));
}

template<class T>
std::pair<double, double> bench_func(const T func, const size_t BENCHMARK_RUNS=1000000) {
  std::vector<double> results(BENCHMARK_RUNS);
  std::chrono::steady_clock::time_point begin, end;
  for (size_t ii=0; ii < BENCHMARK_RUNS; ii++) {
    begin = std::chrono::steady_clock::now();
    func();
    end = std::chrono::steady_clock::now();
    // TODO: automatically determine best units
    results[ii] = static_cast<double>(std::chrono::duration_cast<
                  std::chrono::microseconds>(end-begin).count());
  }
  return find_mean_stdev(results);
}

