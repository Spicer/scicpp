#!/bin/bash

#g++ tests/matrix.cpp -o build/tests/matrix -O2 -pipe -Iinclude -lm \
#    -std=c++17 -Wall -Wextra -pedantic \
#    -ftree-vectorizer-verbose=6
echo "building matrix test"
clang++ tests/matrix.cpp -o build/tests/matrix -O3 -pipe \
    -march=native -mfma -mavx2 -msse2 \
    -funsafe-math-optimizations -ffp-contract=fast -ffast-math \
    -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
    -std=c++17 -Wall -Wextra -pedantic \
    -Iinclude -lm \

echo ""
echo ""
echo "building vector test"
clang++ tests/vector.cpp -o build/tests/vector -O3 -pipe \
    -march=native -mfma -mavx2 -msse2 \
    -funsafe-math-optimizations -ffp-contract=fast -ffast-math \
    -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
    -std=c++17 -Wall -Wextra -pedantic \
    -Iinclude -lm \
