#pragma once

#include <math/size.hpp>
#include <math/vector.hpp>
#include <alloc/aligned_alloc.hpp>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cstring>
#include <cassert>

namespace sci {
template <typename type>
class Matrix {
  public:
   Matrix() = default;

   // size-based constructor
   explicit Matrix(const size_t rows, const size_t cols) :
     _buf(rows * cols), _size({rows, cols})
   {}
   explicit Matrix(const Size size) :
     _buf(size.rows * size.cols), _size(size)
   {}

   // initializer list constructors
   // TODO: use something like std::optional and handle bad sizes passed in
   // ie, sizes that don't match the initializer list given
   explicit Matrix(const std::initializer_list<type>&& l,
                   const size_t rows, const size_t cols) :
     _buf(std::move(l)), _size({rows, cols})
   {}
   explicit Matrix(const std::initializer_list<type>&& l, const Size size) :
     _buf(std::move(l)), _size(size)
   {}
   explicit Matrix(const std::initializer_list<type>&& l) :
     Matrix(std::move(l), l.size(), 1)
   {}

   // copy constructor
   Matrix(const Matrix& mat) noexcept :
     _buf(mat._buf), _size({mat._size.rows, mat._size.cols})
   {}

   // move constructor
   Matrix(const Matrix&& mat) noexcept :
     _buf(std::move(mat._buf)), _size({mat._size.rows, mat._size.cols})
   {}

   Size size() const { return _size; }

   size_t nelements() const { return _buf.size(); }

   // TODO: in-place transpose (?)
   Matrix<type> T() const {
     Matrix<type> result(_size.cols, _size.rows);
     size_t res_idx(0);
     for (size_t row = 0; row < _size.cols; ++row) {
       size_t local_idx(row);
       for (size_t col = 0; col < _size.rows; ++col) {
         result.elem(res_idx) = _buf[local_idx];
         ++res_idx;
         local_idx += _size.cols;
       }
     }

     return result;
   }

   void set(const char val) {
     memset(_buf.data(), val, nelements() * sizeof(type));
   }
   void fill(const type val) {
     std::fill(_buf.begin(), _buf.end(), val);
   }

   // destructor
   ~Matrix() = default;

   Matrix& operator+=(const Matrix& mat2) {
     assert(_size == mat2.size());
     type* res(_buf.data());
     const type* inp(mat2._buf.data());
     #pragma clang loop vectorize(enable) interleave(enable)
     //#pragma clang loop vectorize_width(4) interleave_count(4)
     for (size_t idx = 0; idx < nelements(); ++idx) {
       *res++ += *inp++;
       //_buf[idx] += mat2._buf[idx];
     }
     return *this;
   }
   Matrix<type> operator+(const Matrix& mat2) const {
     Matrix<type> result(*this);
     result += mat2;
     return result;
   }

   // FIXME: this should be elementwise
   // make function for matrix multiplication
   Matrix<type> operator*(const Matrix& mat2) const {
     assert(_size.cols == mat2._size.rows);
     Matrix result(_size.rows, mat2._size.cols);

     // TODO: more efficient algorithm
     for (size_t newcol = 0; newcol < mat2._size.cols; ++newcol) {
       size_t res_idx(newcol);
       size_t idx1(0);
       for (size_t row = 0; row < _size.rows; ++row) {
         size_t idx2(newcol);
         for (size_t col = 0; col < _size.cols; ++col) {
           result.elem(res_idx) += _buf[idx1] * mat2._buf[idx2];
           ++idx1;
           idx2 += mat2._size.cols;
         }
         res_idx += mat2._size.cols;
       }
     }
     return result;
   }

   operator std::string() const {
     std::ostringstream ss;
     const type* buf_ptr(_buf.data());
     for (size_t row = 0; row < _size.rows; ++row) {
       for (size_t col = 0; col < _size.cols; ++col) {
         ss << *buf_ptr++ << ", ";
       }
       ss << std::endl;
     }
     return ss.str();
   }

   bool operator==(const Matrix& mat2) {
     return (_size == mat2._size) && (_buf == mat2._buf);
   }
   Vector<type>& operator[](const size_t ridx) {
     return std::forward(Vector(&_buf[ridx], _size.cols));
   }
   const Vector<type>& operator[](const size_t ridx) const {
     return std::forward(Vector(&_buf[ridx], _size.cols));
   }
   // access a particular element
   // more efficient version of [][]
   type& elem(const size_t elem) { return _buf[elem]; }

   // TODO: write swap function
   Matrix& operator=(Matrix rhs) {
     swap(rhs);
     return *this;
   }

  private:
   Buffer<type> _buf;
   Size _size;
};

template <typename T>
inline bool operator!=(const Matrix<T>& lhs, const Matrix<T>& rhs) {
  return !operator==(lhs,rhs);
}
template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& obj) {
  // write obj to stream
  os << operator std::string(obj);

  return os;
}

} //end namespace sci

