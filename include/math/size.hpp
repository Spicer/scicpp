#pragma once

#include <cstdlib>
#include <string>
#include <sstream>

namespace sci {
struct Size {
  size_t rows;
  size_t cols;

  bool operator==(const Size& s2) {
    return rows == s2.rows && cols == s2.cols;
  }
  operator std::string() const {
    std::ostringstream ss;
    ss << rows << " x " << cols;
    return ss.str();
  }
};

} //end namespace sci

