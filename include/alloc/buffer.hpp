#pragma once

#include <alloc/aligned_alloc.hpp>

#include <algorithm>

namespace sci {
template<typename type, typename alloc_t=AlignedAllocator<type>>
class Buffer {
  public:
   // TODO: is this correct??
   // WARNING: This will take ownership of ptr, so the contents of @param ptr
   // will be deallocated when this class' destructor is called
   // WARNING: you must supply the allocator which allocated the memory
   // so that it may be safely deleted/freed
   explicit Buffer(const type* ptr, const std::size_t size) :
     _ptr(ptr), _size(size)
   {}

   explicit Buffer(const std::size_t size) :
     _size(size)
   {
     _ptr = _allocator.allocate(_size);
   }

   // initializer list constructors
   // TODO: use something like std::optional and handle bad sizes passed in
   // ie, sizes that don't vecch the initializer list given
   explicit Buffer(const std::initializer_list<type>&& l) :
     _size(l.size())
   {
     _ptr = _allocator.allocate(_size);
     std::transform(l.begin(), l.end(), begin(), [](const type& val) { return val; });
   }

   ~Buffer() {
     _allocator.deallocate(_ptr, _size);
   }

   const type* data() const noexcept { return _ptr; }
   type* data() noexcept { return _ptr; }
   // TODO: fancier iterators?
   type* begin() noexcept { return _ptr; }
   type*   end() noexcept { return _ptr + _size; }
   const type* begin() const noexcept { return _ptr; }
   const type*   end() const noexcept { return _ptr + _size; }

   std::size_t size() const noexcept { return _size; }

   // operator overloads
   bool operator==(const Buffer& buf2) {
     return std::equal(begin(), end(), buf2.begin(), buf2.end());
   }

   type& operator[](const size_t idx) { return _ptr[idx]; }
   const type& operator[](const size_t idx) const {
     return _ptr[idx];
   }

  private:
   type* _ptr;
   std::size_t _size;
   alloc_t _allocator;

};  // end class Buffer

template <typename T>
inline bool operator!=(const Buffer<T>& lhs, const Buffer<T>& rhs) {
  return !operator==(lhs,rhs);
}

}   // end namespace sci

