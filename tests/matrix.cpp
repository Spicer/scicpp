#include <math/matrix.hpp>

#include <iostream>
#include <cstdint>
#include <cassert>

using sci::Matrix;
using sci::Size;

bool test_plus() {
  Matrix<uint32_t> mat1({1, 2, 3, 4, 6});
  Matrix<uint32_t> mat2({3, 4, 1, 2, 7});

  mat1 += mat2;
  assert(mat1 == Matrix<uint32_t>({4, 6, 4, 6, 13}));

  Matrix<uint32_t> mat3(mat1 + mat2);
  assert(mat3 == Matrix<uint32_t>({7, 10, 5, 8, 20}));

  return true;
}

bool test_mult() {
  Matrix<uint32_t> mult1({1, 3, 5}, 1, 3);
  Matrix<uint32_t> mult2({
      1, 5,
      3, 7,
      5, 9
    }, 3, 2
  );

  Matrix<uint32_t> res1(mult1 * mult2);
  assert(res1 == Matrix<uint32_t>({35, 71}, 1, 2));

  Matrix<uint32_t> mult3({
      1, 2, 3,
      4, 5, 6,
      7, 8, 9
    }, 3, 3
  );
  Matrix<uint32_t> mult4({
      9, 8, 7, 6,
      5, 4, 3, 2,
      1, 10, 11, 12
    }, 3, 4
  );
  Matrix<uint32_t> res2(mult3 * mult4);
  assert(
      res2 ==
      Matrix<uint32_t>({
            22,  46,  46,  46,
            67,  112, 109, 106,
            112, 178, 172, 166
          }, 3, 4
      )
  );

  return true;
}

bool test_transpose() {
  Matrix<uint32_t> mat({
        22,  46,  46,  46,
        67,  112, 109, 106,
        112, 178, 172, 166
      }, 3, 4
  );

  Matrix<uint32_t> res(mat.T());
  assert(
      res ==
      Matrix<uint32_t>({
          22, 67,  112,
          46, 112, 178,
          46, 109, 172,
          46, 106, 166
        }, 4, 3
      )
  );

  return true;
}

int main() {
  test_plus();
  test_mult();
  test_transpose();

  std::cout << "Size is ";
  if (!std::is_pod<Size>::value) {
    std::cout << "not ";
  }
  std::cout << "pod" << std::endl;
  std::cout << "tests completed!!" << std::endl;
  return 0;
}
