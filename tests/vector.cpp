#include <math/vector.hpp>

#include <iostream>
#include <cstdint>
#include <cassert>

using sci::Vector;

bool test_plus() {
  Vector<uint32_t> vec1({1, 2, 3, 4, 6});
  Vector<uint32_t> vec2({3, 4, 1, 2, 7});

  vec1 += vec2;
  assert(vec1 == Vector<uint32_t>({4, 6, 4, 6, 13}));

  Vector<uint32_t> vec3(vec1 + vec2);
  assert(vec3 == Vector<uint32_t>({7, 10, 5, 8, 20}));

  return true;
}

bool test_mult() {
  Vector<uint32_t> mult1({1, 3, 5});
  Vector<uint32_t> mult2({1, 5, 9});

  Vector<uint32_t> res1(mult1 * mult2);
  assert(res1 == Vector<uint32_t>({1, 15, 45}));

  Vector<uint32_t> mult3({
      1, 2, 3, 4, 5, 6, 7, 8, 9
    }
  );
  Vector<uint32_t> mult4({
      9, 8, 7, 6, 5, 4, 3, 2, 1
    }
  );
  Vector<uint32_t> res2(mult3 * mult4);
  assert(
      res2 ==
      Vector<uint32_t>({
          9, 16, 21, 24, 25, 24, 21, 16, 9
        }
      )
  );

  return true;
}

int main() {
  test_plus();
  test_mult();

  std::cout << "tests completed!!" << std::endl;
  return 0;
}
